# fileio-extra

## 介绍
fileio-extra封装了ohos.fileio的接口，扩展了fileio的能力，相比于fileio,提供了更丰富全面的文件操作功能：

- 创建文件/文件夹。
- 删除文件/文件夹。
- 移动文件/文件夹（可选择是否覆盖同名文件/文件夹）。
- 读写文件。
- 清空文件夹。
- 拷贝文件/文件夹。
- 判断文件/文件夹是否存在。

## 下载安装

```shell
ohpm install @ohos/fileio-extra
```
- OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

### 1.创建文件夹目录

```typescript
    import fs from '@ohos/fileio-extra'
    //同步创建
    fs.mkdirsSync("xx/xx/dirname") //目录路径/文件夹名
    //异步创建
    fs.mkdirs("xx/xx/dirname").then(() => {
        console.log('创建成功')
    }).catch(err => {
        console.log('创建失败' + err)
    })
```

### 2.创建 txt文件和 json文件

```typescript
    import fs from '@ohos/fileio-extra'
    //同步创建 txt文件和 json文件
    fs.outputFileSync("xx/xx/filename.txt", '文件内容') //目录路径/文件名.txt
    fs.outputJSONSync("xx/xx/filename.json", '{}', { encoding: "utf-8"}) //目录路径/文件名.json
    //异步创建 txt文件和 json文件
    fs.outputFile("xx/xx/filename.txt", '文件内容').then(() => {
        console.log('创建成功')
    }).catch(err => {
        console.log('创建失败' + err)
    })
    fs.outputJSON("xx/xx/filename.json", '{}', { encoding: "utf-8"}).then(() => {
        console.log('创建成功')
    }).catch(err => {
        console.log('创建失败' + err)
    })
```

### 3.删除某文件夹目录或文件

```typescript
    import fs from '@ohos/fileio-extra'
    //同步删除
    fs.removeSync("xx/xx/filename") //需要删除的文件夹目录或文件路径
    //异步删除
    fs.remove("xx/xx/filename").then(() => {
        console.log('删除成功')
    }).catch(err => {
        console.log('删除失败' + err)
    })
```

### 4.拷贝某文件夹目录或文件

```typescript
    import fs from '@ohos/fileio-extra'
    //同步拷贝（参数一：需要拷贝的文件路径，参数二：目标路径）
    fs.copySync("xx/folder1/filename", "xx/folder2/filename")
    //异步拷贝
    fs.copy("xx/folder1/filename", "xx/folder2/filename").then(() => {
        console.log('拷贝成功')
    }).catch(err => {
        console.log('拷贝失败' + err)
    })
```

### 5.移动某文件夹目录或文件

```typescript
    import fs from '@ohos/fileio-extra'
    //同步移动（参数一：需要移动的文件路径，参数二：目标路径）
    fs.moveSync("xx/folder1/filename", "xx/folder2/filename")
    //异步移动
    fs.move("xx/folder1/filename", "xx/folder2/filename").then(() => {
        console.log('移动成功')
    }).catch(err => {
        console.log('移动失败' + err)
    })
```

### 6.判断某文件夹或文件是否存在

```typescript
    import fs from '@ohos/fileio-extra'
    //同步判断（存在为true，不存在为false）
    let path = fs.pathExistsSync("xx/folder1/filename") //文件夹或文件路径
    console.log(path + ' = true或false')
    //异步判断
    fs.pathExists("xx/folder1/filename").then((res) => {
        console.log('存在为true，不存在为false' + res)
    })
```

### 6.清空某文件夹下所有文件或文件夹

```typescript
    import fs from '@ohos/fileio-extra'
    //同步清空
    fs.emptyDirSync("xx/folder1/filename") //文件夹路径(路径为文件时报错)
    //异步清空
    fs.emptyDir("xx/folder1/filename").then((res) => {
        console.log('清空成功')
    })
```

## 接口说明

### fileio-extra
|方法名|入参|返回值|接口描述|
|:---:|:---:|:---:|:---:|
|copy|src:string 文件路径 <br/> dest:string 目标路径 <br/> [options](#options)?:object |无|同步拷贝文件或文件夹<br/>(src为文件时dest也必须为文件)<br/>(src为目录时dest也必须为目录)|
|copy|src:string 文件路径 <br/> dest:string 目标路径 <br/> [options](#options)?:object <br/> cb?()=>{} 拷贝成功回调|无|异步拷贝文件或文件夹<br/>(src为文件时dest也必须为文件)<br/>(src为目录时dest也必须为目录)|
|emptyDirSync|dir:string 文件夹路径|无|同步清空文件夹|
|emptyDir|dir:string 文件夹路径|无|异步清空文件夹|
|outputJSONSync|file:string json文件路径<br/>data:string json文件内容 <br/> [options](#options)?:object|无|同步创建json文件|
|outputJSON|file:string json文件路径<br/>data:string json文件内容 <br/> [options](#options)?:object|无|异步创建json文件|
|mkdirsSync|dir:string 文件夹路径<br/> [mode](#mode)?:number|无|同步创建文件夹|
|mkdirs|dir:string 文件夹路径<br/> [mode](#mode)?:number|无|异步创建文件夹|
|moveSync|src:string 文件路径<br/> dest:string 目标路径<br/> [opts](#opts)?:object|无|同步移动文件|
|move|src:string 文件路径<br/> dest:string 目标路径<br/> [opts](#opts)?:object<br/> cb?()=>{} 移动成功回调|无|异步移动文件<br/>(src为文件时dest也必须为文件)<br/>(src为目录时dest也必须为目录)|
|outputFileSync|file:string 文件路径<br/>data:string 文件内容<br/> [options](#options)?:object|无|同步创建文件夹|
|outputFile|file:string 文件路径<br/>data:string 文件内容<br/> [options](#options)?:objectcb?()=>{} 创建成功回调|无|异步创建文件夹|
|pathExistsSync|path:string 文件路径|true/false|同步判断文件是否存在|
|pathExists|path:string 文件路径|true/false|异步判断文件是否存在|
|removeSync|path:string 文件路径|无|同步删除文件|
|remove|path:string 文件路径|无|异步删除文件|

### options
支持如下选项：
- offset，number类型，表示期望写入数据的位置相对于数据首地址的偏移。可选，默认为0。
- length，number类型，表示期望写入数据的长度。可选，默认缓冲区长度减去偏移量长度。
- position，number类型，表示期望写入文件的位置。可选，默认为当前位置开始写。
- encoding，string类型，当数据是string类型时有效，表示期望数据的编码方式。可选，默认'utf-8'。
 约束：offset+length<=buffer.size。

### mode
创建目录的权限，可给定如下权限，以按位或的方式追加权限，默认给定0o775。
- 0o775：所有者具有读、写及可执行权限，其余用户具有读、写及可执行权限。
- 0o700：所有者具有读、写及可执行权限。
- 0o400：所有者具有读权限。
- 0o200：所有者具有写权限。
- 0o100：所有者具有可执行权限。
- 0o070：所有用户组具有读、写及可执行权限。
- 0o040：所有用户组具有读权限。
- 0o020：所有用户组具有写权限。
- 0o010：所有用户组具有可执行权限。
- 0o007：其余用户具有读、写及可执行权限。
- 0o004：其余用户具有读权限。
- 0o002：其余用户具有写权限。
- 0o001：其余用户具有执行权限。

### opts
支持如下选项：
- overwrite，true/false 表示是否覆盖目标文件或文件夹。可选，默认为false。

## 约束与限制
在下述版本验证通过：

DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## 目录结构
````
|----fileio-extra
|     |---- entry  # 示例代码文件夹
|     |---- FileioExtra  # fileio-extra库逻辑代码
|                |---- fileio-extra # 文件操作
|                      |---- copy # 文件拷贝
|                      |---- empty # 目录清空
|                      |---- ensure # 创建文件
|                      |---- fs # 封装系统ohos.fileio接口
|                      |---- json # json文件操作
|                      |---- mkdirs # 创建文件夹
|                      |---- move # 移动文件
|                      |---- output-file # 写入文件
|                      |---- path-exists # 判断路径是否存在
|                      |---- remove # 删除文件
|                      |---- util # 文件信息
|                |---- jsonfile # json操作
|                |---- node # 部分node实现
|                |---- univresalify # 回调函数实现
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/fileio-extra/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/fileio-extra/pulls) 。

## 开源协议
本项目基于 [The MIT License](https://gitee.com/openharmony-sig/fileio-extra/blob/master/LICENSE) ，请自由地享受和参与开源。 
    



