import rimraf from './rimraf'
import { fromCallback } from '../../universalify'

export default {
    remove: fromCallback(rimraf.rimraf),
    removeSync: rimraf.rimrafSync
}