import { fromCallback } from '../../universalify'
import move from './move'
import moveSync from './move-sync'

export default {
    move: fromCallback(move),
    moveSync
}