import jsonFile from './jsonfile'
import { fromPromise } from "../../universalify"
import { outputJson } from "./output-json"
import { outputJsonSync } from "./output-json-sync"

export default {
    outputJSON: fromPromise(outputJson),
    outputJSONSync: outputJsonSync,
    writeJSON: jsonFile.writeJson,
    writeJSONSync: jsonFile.writeJsonSync,
    readJSON: jsonFile.readJson,
    readJSONSync: jsonFile.readJsonSync
}
