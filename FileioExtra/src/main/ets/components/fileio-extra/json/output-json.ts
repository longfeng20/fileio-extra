import { stringify } from '../../jsonfile/utils'
import outputFile from '../output-file'

export function outputJson (file, data, options = {}) {
  return new Promise((res, rej) => {
    const str = stringify(data, options)
    res(outputFile.outputFile(file, str, options))
  })
}