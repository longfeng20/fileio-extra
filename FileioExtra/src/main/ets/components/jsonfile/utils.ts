export function stringify(obj, { EOL = '\n', finalEOL = true, replacer = null, spaces = null } = {}) {
  const EOF = finalEOL ? EOL : ''
  const str = JSON.stringify(obj, replacer, spaces)
  return str.replace(/\n/g, EOL) + EOF
}